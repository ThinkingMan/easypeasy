make: tex/* easypeasy.tex
	latexmk -pdf easypeasy.tex

.PHONY: clean

clean:
	rm easypeasy.aux easypeasy.log easypeasy.toc easypeasy.fdb_latexmk easypeasy.fls



